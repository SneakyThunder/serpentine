# Serpentine

> My personal grimoire for [SourceMage](https://sourcemage.org) GNU/Linux

### Purpose

The purposes of this grimoire is
1. Solve problems I had with official grimoires (thats why you can find here some spells that exist in official grimoires)
2. Have updated versions of some spells
3. Add some missing spells that I find useful

### Install

**Warning:** this grimoire intended to work with `test` branch of official grimoire
(at the time of writing `stable` branch is too out-of-date)

```
scribe add serpentine from git://gitlab.com/SneakyThunder/serpentine.git
scribe swap test serpentine	# If you want to use updated/fixed spells
```

### Credits

This repo contains code from [SourceMage developers](https://sourcemage.org/Developers)
